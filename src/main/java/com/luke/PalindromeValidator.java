package com.luke;

public class PalindromeValidator {

  private static final String REGEX_TO_MATCH = "[^\\p{IsAlphabetic}]";

  public boolean isPalindrome(String stringToValidate) {
    if (stringToValidate == null)
      return false;

    // Based on example of "A Toyota’s a Toyota", seems like we are ignoring non-alpha chars
    stringToValidate = cleanInputAndConvertToUpperCase(stringToValidate);

    if (stringToValidate.isEmpty())
      return false;

    return doPalindromeCheck(stringToValidate);
  }

  private boolean doPalindromeCheck(String stringToValidate) {
    char[] inputChars = stringToValidate.toCharArray();
    int inputEndPos = stringToValidate.length() -1;

    // NOTE: This will skip the middle character in a odd-length string - don't need to check it
    for (int i = 0; i < stringToValidate.length() / 2; i++) {
      char charFromFront = inputChars[i];
      char charFromEnd = inputChars[inputEndPos - i];

      if (charFromFront != charFromEnd) {
        System.out.println("Non-matching chars are " + charFromFront + " and " + charFromEnd +
            "found at index " + i + " and " + (inputEndPos - i));
        return false;
      }

    }

    return true;
  }

  private String cleanInputAndConvertToUpperCase(String stringToValidate) {
    stringToValidate = stringToValidate.replaceAll(REGEX_TO_MATCH,"");

    // Using lower case because characters like ß get converted to a normal capital 'S' when upper'd
    return stringToValidate.toLowerCase();
  }

}
