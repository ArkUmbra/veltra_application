package com.luke;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AddToZero {

  /**
   * @param inputs
   * @return List of unique arrays which sum to zero
   */
  public List<Integer[]> addsToZero(int[] inputs) {
    List<Integer[]> combinations = new ArrayList<>();

    if (inputs == null || inputs.length < 3) {
      return combinations; // not enough numbers so just return empty
    }

    Arrays.sort(inputs);

    int endIndex = inputs.length - 1;
    for (int currentIndex = 0; currentIndex < endIndex; currentIndex++) {
      // Kind of like the palindrome solution where we check from both ends, but
      // let's try getting one small and large number (from left and right of sorted array),
      // and then tweaking depending on whether we're above or below the target answer (zero)

      int leftIndex = currentIndex + 1;
      int rightIndex = endIndex;

      while (leftIndex < rightIndex) {
        int c = inputs[currentIndex];
        int l = inputs[leftIndex];
        int r = inputs[rightIndex];

        if (c + l + r == 0) {
          combinations.add(new Integer[]{c, l, r});
          leftIndex++;
          rightIndex--;
        } else if (c + l + r < 0) {
          leftIndex++; // try with slighter bigger number
        } else if (c + l + r > 0) {
          rightIndex--; // try with slightly smaller number;
        }
      }
    }

    return combinations;

  }

}
