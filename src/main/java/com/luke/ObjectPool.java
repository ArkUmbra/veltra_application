package com.luke;


import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.concurrent.TimeoutException;

public abstract class ObjectPool<T> {
  private final int maxPoolSize;

  private int currentPoolSize;

  private LinkedHashSet<T> unavailable = new LinkedHashSet<>();
  private LinkedList<T> available = new LinkedList<>();

  public ObjectPool(int maxPoolSize) {
    this.maxPoolSize = maxPoolSize;
  }

  public ObjectPool(int maxPoolSize, boolean fillPoolAtCreationTime) {
    this.maxPoolSize = maxPoolSize;

    if (fillPoolAtCreationTime) {
      while (maxPoolSizeNotReached()) {
        T t = createNewObjectAndMarkPoolIncrease();
        available.add(t);
      }
    }
  }

  public synchronized T obtain(int timeoutMs) throws TimeoutException {
    if (! available.isEmpty()) {
      return fetchAvailableObjectAndMarkAsUnavailable();

    } else if (maxPoolSizeNotReached()) {
      T t = createNewObjectAndMarkPoolIncrease();
      unavailable.add(t);
      return t;

    } else {
      waitUntilObjectIsAvailable(timeoutMs);

      return fetchAvailableObjectAndMarkAsUnavailable();
    }
  }

  private T fetchAvailableObjectAndMarkAsUnavailable() {
    T t = available.pop();
    unavailable.add(t);
    return t;
  }

  private boolean maxPoolSizeNotReached() {
    return currentPoolSize < maxPoolSize;
  }

  private T createNewObjectAndMarkPoolIncrease() {
    T t = createObject();
    currentPoolSize++;
    return t;
  }

  private void waitUntilObjectIsAvailable(int timeoutMs) throws TimeoutException {
    long startTimeMillis = System.currentTimeMillis();

    while (available.isEmpty()) {
      try {
        if (startTimeMillis + timeoutMs > System.currentTimeMillis()) {
          throw new TimeoutException("Exceeded wait time of " + timeoutMs + "milliseconds");
        }

        Thread.sleep(50);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  public void release(T objectHasFinished) {
    if (unavailable.contains(objectHasFinished)) {
      unavailable.remove(objectHasFinished);

      if (isValidToBeRepooled(objectHasFinished)) {
        available.add(objectHasFinished);
      } else {
        available.add(createObject());
      }
    } else {
      // don't know this object, because it's not currently on our queue
      // so just ignore
      throw new RuntimeException("Invalid object to release - obtain() not yet called");
    }

  }

  public final int sizeOfAvailable() {
    return available.size();
  }

  public final int sizeOfInUse() {
    return unavailable.size();
  }

  protected abstract T createObject();

  protected abstract boolean isValidToBeRepooled(T t);

}