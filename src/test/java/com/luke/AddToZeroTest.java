package com.luke;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

public class AddToZeroTest {

  private AddToZero sut;

  @Before
  public void setUp() {
    sut = new AddToZero();
  }

  @Test
  public void testExampleOne() {
    int[] input = new int[]{-4,-2,-1,0,1,3,4};
    List<Integer[]> expected = new ArrayList<>();
    expected.add(new Integer[]{-4,1,3});
    expected.add(new Integer[]{-4,0,4});
    expected.add(new Integer[]{-2,-1,3});
    expected.add(new Integer[]{-1,0,1});

    validate(input, expected);
  }

  @Test
  public void testWhenNoMatches() {
    int[] input = new int[]{-6,0,7,18,19,26,27};
    List<Integer[]> expected = new ArrayList<>();

    validate(input, expected);
  }

  @Test
  public void testWhenOnlyTwoNumbers() {
    int[] input = new int[]{-4,-4};
    List<Integer[]> expected = new ArrayList<>();

    validate(input, expected);

  }

  @Test
  public void testWhenMultiples() {
    int[] input = new int[]{0,0,0};
    List<Integer[]> expected = new ArrayList<>();
    expected.add(new Integer[]{0,0,0});

    validate(input, expected);
  }


  private void validate(int[] input, List<Integer[]> expectedAnswers) {
    List<Integer[]> results = sut.addsToZero(input);

    assertEquals(expectedAnswers.size(), results.size());

    for (Integer[] result : results) {
      System.out.println(Arrays.toString(result));

      boolean matchFound = false;
      for (Integer[] expectedAnswer : expectedAnswers) {
        Arrays.sort(result);
        Arrays.sort(expectedAnswer);
        if (Arrays.equals(result, expectedAnswer)) {
          matchFound = true;
          break;
        }
      }

      if (! matchFound) {
        fail("Didn't find a match for " + Arrays.toString(result));
      }
    }

  }

}
