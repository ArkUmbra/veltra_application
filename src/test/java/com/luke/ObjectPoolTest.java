package com.luke;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.fail;

import java.util.concurrent.TimeoutException;
import org.junit.After;
import org.junit.Test;

public class ObjectPoolTest {

  public ExpensiveOperationConnectionPool subjectUnderTest;

  @After
  public void setUp() {
    this.subjectUnderTest = null;
  }

  @Test
  public void testObtainAndRelease() throws TimeoutException {
    this.subjectUnderTest = new ExpensiveOperationConnectionPool(3);

    // 0, because didn't fill available pool at startup
    assertEquals(0, subjectUnderTest.sizeOfAvailable());
    assertEquals(0, subjectUnderTest.sizeOfInUse());

    ExpensiveOperation op = subjectUnderTest.obtain(5000);

    assertEquals(0, subjectUnderTest.sizeOfAvailable());
    assertEquals(1, subjectUnderTest.sizeOfInUse());

    op.doWork();
    subjectUnderTest.release(op);

    assertEquals(1, subjectUnderTest.sizeOfAvailable());
    assertEquals(0, subjectUnderTest.sizeOfInUse());

    ExpensiveOperation op2 = subjectUnderTest.obtain(5000);
    // Have now called 'obtain' twice, but has reused the same object because the prev call released
    assertEquals(0, subjectUnderTest.sizeOfAvailable());
    assertEquals(1, subjectUnderTest.sizeOfInUse());

    ExpensiveOperation op3 = subjectUnderTest.obtain(5000);
    assertEquals(0, subjectUnderTest.sizeOfAvailable());
    assertEquals(2, subjectUnderTest.sizeOfInUse());

    ExpensiveOperation op4 = subjectUnderTest.obtain(5000);
    assertEquals(0, subjectUnderTest.sizeOfAvailable());
    assertEquals(3, subjectUnderTest.sizeOfInUse());

    subjectUnderTest.release(op2);
    assertEquals(1, subjectUnderTest.sizeOfAvailable());
    assertEquals(2, subjectUnderTest.sizeOfInUse());

    subjectUnderTest.release(op3);
    assertEquals(2, subjectUnderTest.sizeOfAvailable());
    assertEquals(1, subjectUnderTest.sizeOfInUse());

    subjectUnderTest.release(op4);
    assertEquals(3, subjectUnderTest.sizeOfAvailable());
    assertEquals(0, subjectUnderTest.sizeOfInUse());

    ExpensiveOperation op5 = subjectUnderTest.obtain(5000);
    assertEquals(2, subjectUnderTest.sizeOfAvailable());
    assertEquals(1, subjectUnderTest.sizeOfInUse());
  }

  @Test
  public void testMaxPoolSize() throws TimeoutException {
    this.subjectUnderTest = new ExpensiveOperationConnectionPool(3);

    ExpensiveOperation op = subjectUnderTest.obtain(5000);
    ExpensiveOperation op2 = subjectUnderTest.obtain(5000);
    ExpensiveOperation op3 = subjectUnderTest.obtain(5000);

    try {
      subjectUnderTest.obtain(5000);
      fail("Expected exception because objects not been returned to pool");
    } catch (TimeoutException t) {
      // indicates max pool size working
    }
  }

}


class ExpensiveOperationConnectionPool extends ObjectPool<ExpensiveOperation> {

  public ExpensiveOperationConnectionPool(int maxPoolSize) {
    super(maxPoolSize, false);
  }

  @Override
  public ExpensiveOperation createObject() {
    return new ExpensiveOperation();
  }

  @Override
  protected boolean isValidToBeRepooled(ExpensiveOperation expensiveOperation) {
    // do something relevant to your object here
    return expensiveOperation.isConnected();
  }

}

class ExpensiveOperation {
  public void doWork() {
    try {
      Thread.sleep(2000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public boolean isConnected() {
    return true;
  }
}
