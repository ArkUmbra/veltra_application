package com.luke;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class PalindromeValidatorTest {

  private PalindromeValidator sut;

  @Before
  public void before() {
    sut = new PalindromeValidator();
  }

  @Test
  public void testExampleOne() {
    String input = "Hello World";
    boolean isPalindrome = false;

    validate(input, isPalindrome);
  }

  @Test
  public void testExampleTwo() {
    String input = "A Toyota's a Toyota";
    boolean isPalindrome = true;

    validate(input, isPalindrome);
  }

  @Test
  public void testWithOddNumberOfChars() {
    String input = "ABCDCBA";
    boolean isPalindrome = true;

    validate(input, isPalindrome);
  }

  @Test
  public void testWithEvenNumberOfChars() {
    String input = "ABCDDCBA";
    boolean isPalindrome = true;

    validate(input, isPalindrome);
  }

  @Test
  public void testWithNonLatinAlphabet() {
    String input = "ßacaß";
    boolean isPalindrome = true;

    validate(input, isPalindrome);
  }

  @Test
  public void testShort() {
    String input = "ßA";
    boolean isPalindrome = false;

    validate(input, isPalindrome);
  }


  @Test
  public void testVeryLongPalindrome() {
    String input = "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "AAAADESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED" +
        "DESSERTS I DESIRE NOT SO LONG NO LOST ONE RISE DISTRESSED"
        ;
    boolean isPalindrome = false;

    validate(input, isPalindrome);
  }

  @Test
  public void testEmpty() {
    String input = "";
    boolean isPalindrome = false;

    validate(input, isPalindrome);
  }

  private void validate(String input, boolean expectedIsPalindrome) {
    System.out.println("\n\nInput is " + input);

    boolean isPalindrome = sut.isPalindrome(input);
    assertEquals(isPalindrome, expectedIsPalindrome);
  }

}
